#ifndef HWO_GAME_STATE_H
#define HWO_GAME_STATE_H

#define _USE_MATH_DEFINES
#include <cmath>

#include <iostream>
#include <limits>
#include <string>
#include <vector>


const bool correct_radius = true;
const double epsilon = 1e-6;

static inline bool isEqual(double a, double b)
{
    return fabs(a - b) < epsilon;
}

typedef struct
{
    int     region_ndx;
    double  length;
    double  angle;
    double  radius;
    bool    curved;
    bool    can_switch;

} sPiece;

enum    eRegionType { eUndef, eStraight, eLeftCurve, eRightCurve };
typedef struct
{
    eRegionType     region_type;
    int             piece_index_start;
    int             piece_index_stop;
    int             total_pieces;
    int             total_switches;
    double          total_length;
    double          total_angle;
    double          apex;

} sRegion;

typedef struct
{
    std::string                         id;
    bool                                is_initialized;
    bool                                is_quick_race;

    int                                 num_laps;
    int                                 num_lanes;
    int                                 num_pieces;
    int                                 switch_calculated = 0;
    double                              max_lap_time_ms;

    std::vector<sPiece>                 sectors;
    std::vector<double>                 lane_deltas;
    std::vector<std::vector<double>>    sectors_lane_lengths;
    std::vector<sRegion>                regions;

    double piece_length(int curr_piece_index,int lane_index)
    {
        double  curr_length = .0;
        sPiece  curr_piece = sectors[curr_piece_index];
        if(curr_piece.curved)
        {
            curr_length = fabs(curr_piece.angle * M_PI/180.0 * radius(lane_index, curr_piece_index));
        }
        else
        {
            curr_length = curr_piece.length;
        }

        return curr_length;
    }

    double radius(int lane_index, int piece_index)
    {
        double ret = .0;
        sPiece curr_piece = sectors[piece_index];
        if(!correct_radius)
        {
            ret = curr_piece.radius;
        }
        else
        {
            double lane_delta = lane_deltas[lane_index];

            // correct radius to account for current lane offset from center
            bool    lc_lt = (curr_piece.angle < .0 && lane_delta < .0);
            bool    rc_rt = (curr_piece.angle > .0 && lane_delta > .0);

            double  delta_sign = ((lc_lt || rc_rt) ? -1.0 : 1.0);
            ret = curr_piece.radius + delta_sign * fabs(lane_delta);
        }

        return ret;
    }

} sTrack;


typedef struct
{
    // data available from json
    double  curr_lap, prev_lap;
    double  curr_lane_index, prev_lane_index;
    double  curr_piece_index, prev_piece_index, next_piece_index;
    double  max_distance = 0;
    int  max_dist_idx = 0;
    bool turbo_available = false;
    bool can_turbo = false;
    // physics data available from json
    double  curr_t, prev_t;
    double  curr_piece_dx, prev_piece_dx;
    double  curr_drift_angle, prev_drift_angle;

    void update_current(int lap_index, int piece_index, int lane_index, double piece_dx, double drift_angle, double time)
    {
        curr_lap            = lap_index;
        curr_lane_index     = lane_index;
        curr_piece_index    = piece_index;
        curr_piece_dx       = piece_dx;

        curr_t              = time;
        curr_drift_angle    = drift_angle;
    }

    void update_previous()
    {
        prev_lap            = curr_lap;
        prev_lane_index     = curr_lane_index;
        prev_piece_index    = curr_piece_index;


        prev_t              = curr_t;
        prev_piece_dx       = curr_piece_dx;
        prev_drift_angle    = curr_drift_angle;
    }

} sGameState;



class cPhysics
{

public:

    cPhysics() : mu_defined(false), mu(.0) {}

public:

    sTrack      track;
    double      mu;
    bool        mu_defined;
    sGameState  state;

private:
    // physics exrapolated data
    double  curr_v, prev_v;
    double  curr_a, prev_a;
    double  curr_ac, prev_ac;


public:

    double  get_v()             { return curr_v; }
    double  get_a()             { return curr_a; }
    double  get_ac()            { return curr_ac; }

    bool    is_last_lap()       { return ((state.curr_lap == track.num_laps-1) ? true : false); }
    bool    is_last_piece()     { return (state.curr_piece_index == track.num_pieces-1 ? true : false); }
    int     next_piece_index()  { return (is_last_piece() ? 0 : state.curr_piece_index+1); }

    int nextCurveNdx()
    {
        int next_curve_start_ndx = -1;
        int curr_region_ndx = curr_piece()->region_ndx;
        for(size_t r = curr_region_ndx+1; r < track.regions.size(); ++r)
        {
            if(track.regions[r].region_type == eLeftCurve || track.regions[r].region_type == eRightCurve)
            {
                next_curve_start_ndx = track.regions[r].piece_index_start;
                break;
            }
        }

        return next_curve_start_ndx;
    }

    double distanceToNextCurve()
    {
        double distance = track.sectors_lane_lengths[state.curr_piece_index][state.curr_lane_index] - state.curr_piece_dx;
        int next_curve_start_ndx = nextCurveNdx();
        for(int p = state.curr_piece_index; p < next_curve_start_ndx; ++p)
            distance += track.sectors_lane_lengths[state.curr_piece_index][state.curr_lane_index];

        return distance;
    }

    double maxVelocityCurrCurve()
    {
        int curr_curve_start_ndx = state.curr_piece_index;
        double max_v = std::numeric_limits<double>::max();
        if(curr_curve_start_ndx > 0)
            max_v = sqrt(9.8 * 2 * mu * track.radius(state.curr_lane_index, curr_curve_start_ndx));

        return max_v;
    }

    double maxVelocityNextCurve()
    {
        int next_curve_start_ndx = nextCurveNdx();
        double max_v = std::numeric_limits<double>::max();
        if(next_curve_start_ndx > 0)
            max_v = sqrt(9.8 * 2 * mu * track.radius(state.curr_lane_index, next_curve_start_ndx));

        return max_v;
    }

    sPiece* curr_piece()
    {
        return &track.sectors[state.curr_piece_index];
    }

    sPiece* next_piece()
    {
        return &track.sectors[next_piece_index()];
    }

    void update(int lap_index, int piece_index, int lane_index, double piece_dx, double drift_angle, double time)
    {
        state.update_current(lap_index, piece_index, lane_index, piece_dx, drift_angle, time);

        curr_v    = velocity();
        curr_a    = acceleration();

        // strikles uber amazing algo
        // => all teh right friends and all teh right connections in a box of size dx

        // F = mx''
        // x'' = a - k * x'

        // create a system of equations for t-1 and t
        // x''(t-1) = a - kx'(t-1)
        // x''(t)   = a - kx'(t)

        // a = x''(t-1) + kx'(t-1)
        // k = (x''(t) - x''(t-1)) / (x'(t-1) -x'(t))

        if(!mu_defined && curr_v > .0 && prev_v > .0 && curr_a > .0 && prev_a > .0)
        {
            mu = (curr_a - prev_a) / (prev_v - curr_v);
            mu_defined = true;
        }

        curr_ac   = .0;
        if (curr_piece()->curved)
            curr_ac = centripetal_accel(curr_v, track.radius(state.curr_lane_index, state.curr_piece_index));
    }

    void update_previous()
    {
        state.update_previous();
        prev_v  = curr_v;
        prev_a  = curr_a;
        prev_ac = curr_ac;
    }
    double optimal_throttle()
    {
        /*==================================================
        =            Determine optimal throttle            =
        ==================================================*/

        /*==========  Define all the things  ==========*/
        
        double throttle = 1.0;
        double break_friction = 0.014;
        if (mu_defined) {
            break_friction = mu-0.008;
        }
        double curr_target_v = maxVelocityCurrCurve()- 0.3;
        double target_v = maxVelocityNextCurve() -0.6;


        double distance = distanceToNextCurve() + track.piece_length(nextCurveNdx(), 0)*0.3;
        double break_distance = (curr_v-target_v)/break_friction;
        double err_margin =  3 * curr_v;

        if (distance > state.max_distance && state.curr_piece_index > 5) { 
            state.max_dist_idx = state.curr_piece_index;
            state.max_distance = distance;
        }
       //std::cout << distance-break_distance << std::endl ;
        /*==========  On a straight piece  ==========*/
        
        if (!curr_piece()->curved) {
            throttle = 1;
        }

        /*==========  In a corner  ==========*/
        else {
            throttle = 0;
            //if we are below theshold speed
            if (curr_v < curr_target_v ){

                throttle = curr_target_v/10;
                /*
                bool curve_direction_change = false;
                
                if ((track.regions[curr_piece()->region_ndx].region_type == eRightCurve || track.regions[next_piece()->region_ndx].region_type == eRightCurve) && state.curr_drift_angle > 0.0) {
                    curve_direction_change = true;
                }
                if ((track.regions[curr_piece()->region_ndx].region_type == eLeftCurve || track.regions[next_piece()->region_ndx].region_type == eLeftCurve) && state.curr_drift_angle < 0.0) {
                    curve_direction_change = true;              
                }
                */               
            }
            if (  0 <  fabs(state.curr_drift_angle) - fabs(state.prev_drift_angle) ) {
                throttle = 1;
            }

            if (break_distance > 0 && distance > 0 && break_distance > distance - err_margin) {
                // std::cout << "breaking for next curve \n";
                throttle = 0;
            } 


            //if angle is decreasing 
            /*
            else if (fabs(state.curr_drift_angle) <  fabs(state.prev_drift_angle) ) {

                throttle = 1;

                bool curve_direction_change = false;
                
                if ((track.regions[curr_piece()->region_ndx].region_type == eRightCurve || track.regions[next_piece()->region_ndx].region_type == eRightCurve) && state.curr_drift_angle < 0.0) {
                    curve_direction_change = true;
                }
                if ((track.regions[curr_piece()->region_ndx].region_type == eLeftCurve || track.regions[next_piece()->region_ndx].region_type == eLeftCurve) && state.curr_drift_angle > 0.0) {
                    curve_direction_change = true;              
                }
                
                // if the curve is in a different direction than the current drift angle
                
                if (curve_direction_change) {
                    throttle = 0;
                    // std::cout << "breaking for switch piece \n";
                }
                else if (break_distance > 0 && distance > 0 && break_distance < distance - err_margin ) {
                    throttle = target_v/10;
                  //  std::cout << "breaking for next curve \n";
                }

            }   
            */      
        }
  
        /*==========  General pieces  ==========*/


        if (break_distance > 0 && distance > 0 && break_distance > distance - err_margin) {
           // std::cout << "breaking for next curve \n";
            throttle = 0;
        }


        /*-----  End of Determine optimal throttle  ------*/
        
         

        /*
        if(next_piece()->curved)
        {
            if(get_v() > 7.5)
                throttle = .01;
            else if(state.curr_piece_dx > track.piece_length(state.curr_piece_index, state.curr_lane_index)*0.5)
                throttle = 0.6568;
        }

        if (curr_piece()->curved)
        {
            throttle = 0.65;
            if(state.curr_piece_dx < track.piece_length(state.curr_piece_index, state.curr_lane_index)*0.5)
            {
                throttle = .6;
                if(get_v() > 7)
                    throttle = .01;
            }
            else if (!next_piece()->curved)
                throttle = 1.0;
        }

        if(fabs(state.curr_drift_angle) > 7)
        {
            throttle = .6568;
            if(get_v() > 6)
                throttle = .001;

            if(fabs(state.curr_drift_angle) > 14)
                throttle = .0001;

            if(fabs(state.curr_drift_angle) < fabs(state.prev_drift_angle))
                throttle = 0.65682;
        }
        */  
        if (curr_v > 10) {
            throttle = 0;
        }
        return throttle;
    }

    bool do_turbo(){
        if (state.turbo_available && state.curr_piece_index >= state.max_dist_idx && state.curr_piece_index <= state.max_dist_idx+5 && state.curr_piece_dx > track.piece_length(state.curr_piece_index, state.curr_lane_index)*0.8 && !next_piece()->curved) {
            state.turbo_available = false;
            return true;
        }
        return false;
    }
    void make_turbo_available(){
        state.turbo_available = true;
    }
    void make_can_turbo(){
        state.can_turbo = true;
    }


    double dt() { return state.curr_t - state.prev_t; }
    double dx()
    {
        double ret = .0;
        if(state.curr_lap == -1)
            ret = state.curr_piece_dx;
        else if(state.curr_piece_index == state.prev_piece_index)
            ret = state.curr_piece_dx - state.prev_piece_dx;
        else if(state.curr_lap != state.prev_lap)
            ret = track.sectors_lane_lengths[track.num_pieces-1][state.curr_lane_index] - state.prev_piece_dx + state.curr_piece_dx;
        else
            ret = track.sectors_lane_lengths[state.curr_piece_index-1][state.curr_lane_index] - state.prev_piece_dx + state.curr_piece_dx;

        return ret;
    }
    double dv()                                     { return curr_v - prev_v; }
    double velocity()                               { return dt() > 0 ? dx() / dt() : .0; }
    double acceleration()                           { return dt() > 0 ? dv() / dt() : .0; }
    double centripetal_accel(double v, double r)    { return r > 0 ? v*v/r : .0; }
};

#endif
