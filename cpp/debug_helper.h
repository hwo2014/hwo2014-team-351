#ifndef HWO_DEBUG_HELPER_H
#define HWO_DEBUG_HELPER_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>

void debug_helper(const jsoncons::json& data);

#endif

