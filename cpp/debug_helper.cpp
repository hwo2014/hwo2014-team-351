#include "debug_helper.h"

void debug_helper(const jsoncons::json& data)
{
    std::cout << "object? " << data.is<jsoncons::json::object>()
    << ", or array? " << data.is<jsoncons::json::array>() << std::endl;

    if (data.is<jsoncons::json::array>())
    {
        for (size_t i = 0; i < data.size(); ++i)
        {
            const auto& elem = data[i];
            std::cout << "Is " << i << " an object? " << elem.is<jsoncons::json::object>() << std::endl;

            if (elem.is<jsoncons::json::object>())
            {
                for (auto it = elem.begin_members(); it != elem.end_members(); ++it)
                {
                    std::cout << "Is member " << it->name()
                    << " a string? " << it->value().is<std::string>()
                    << ", or a double? " << it->value().is<double>()
                    << ", or perhaps an int? " << it->value().is<int>()
                    << std::endl;

                }
            }
        }
    }
    else if (data.is<jsoncons::json::object>())
    {
        for (auto it = data.begin_members(); it != data.end_members(); ++it)
        {
            std::cout
            << "Is member " << it->name()
            << " a string? " << it->value().is<std::string>()
            << ", or a double? " << it->value().is<double>()
            << ", or perhaps an int? "
            << it->value().is<int>()
            << std::endl;

        }
    }
}

