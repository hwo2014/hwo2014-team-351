#include "game_logic.h"
#include "debug_helper.h"
#include "protocol.h"

using namespace hwo_protocol;

game_logic::game_logic()
: action_map
{
    { "yourCar", &game_logic::on_your_car },
    { "gameInit", &game_logic::on_game_init },
    { "turboAvailable", &game_logic::on_turbo_available },
    { "finish", &game_logic::on_finish },
    { "lapFinished", &game_logic::on_lap_finished },
    { "tournamentEnd", &game_logic::on_tournament_end },
    { "join", &game_logic::on_join },
    { "gameStart", &game_logic::on_game_start },
    { "carPositions", &game_logic::on_car_positions },
    { "crash", &game_logic::on_crash },
    { "spawn", &game_logic::on_spawn },
    { "gameEnd", &game_logic::on_game_end },
    { "error", &game_logic::on_error }
}
{
    model.track.is_initialized = false;
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
    const auto& msg_type    = msg["msgType"].as<std::string>();
    const auto& data        = msg["data"];

    auto action_it = action_map.find(msg_type);
    if (action_it != action_map.end())
    {
        //std::cout << pretty_print(msg) << std::endl;
        if(msg.has_member("gameTick"))
        {
            tick = msg["gameTick"].as<int>();
        }

        return (action_it->second)(this, data);
    }
    else
    {
        std::cout << "Unknown message type: " << msg_type << std::endl;
        return { make_ping() };
    }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
    std::cout << "Joined" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
    std::cout << "Race started" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
    double throttle;
    static int err_count = 0;
    bool do_turbo = false;

    if(!model.track.is_initialized)
    {
        err_count++;
        std::cout << err_count << " Error: Track Uninitialized! " << model.track.num_pieces << std::endl;
    }
    else
    {
        // Get piece data
        // Fix me - our index might not be 0 on the real race
        jsoncons::json data_info    = data[0];
        jsoncons::json piece_info   = data_info["piecePosition"];

        double  drift_angle         = data_info["angle"].as<double>();
        double  piece_dx            = piece_info["inPieceDistance"].as<double>();
        int     piece_index         = piece_info["pieceIndex"].as<int>();
        int     lap_index           = piece_info["lap"].as<int>();
        int     start_lane_index    = piece_info["lane"]["startLaneIndex"].as<int>();
        int     end_lane_index      = piece_info["lane"]["endLaneIndex"].as<int>();

        model.update(lap_index, piece_index, start_lane_index, piece_dx, drift_angle, tick);
        throttle = model.optimal_throttle();
        do_turbo    = model.do_turbo();
        /*
        if(start_lane_index != end_lane_index)
            std::cout << ">> Switching lanes " << std::endl;
        */

        // Debug Output
        /*
        std::cout   << tick
        << " l: ("  << model.state.curr_lane_index      << "),"
        << " L: ["  << model.state.curr_lap             << "/"  << model.track.num_laps-1   << "],"
        << " P: ["  << model.state.curr_piece_index+1   << "/"  << model.track.num_pieces   << "],"
        << " D: ["  << model.state.curr_piece_dx        << "/"  << model.track.sectors_lane_lengths[model.state.curr_piece_index][model.state.curr_lane_index] << "]"
        << std::endl;
        */

    //    std::cout << " [" << model.state.curr_piece_index << "/" << model.track.num_pieces-1 << "], " << throttle  << ", "<< model.get_v()  << ", "<< drift_angle << std::endl;

        std::cout   << model.mu << " [" << (model.curr_piece()->curved ? "C " : (model.curr_piece()->can_switch ? "S " : "- "))
        << tick << ", " << model.state.curr_piece_index << "/" << model.track.num_pieces-1 << "], "
        << throttle         << ", "
        << model.get_v()    << ", "
        << model.get_a()    << ", "
        << model.get_ac()   << ", "
        << drift_angle
        << std::endl;
        // End debug stuff

        model.update_previous();
    }

    if (do_turbo){
        return { make_turbo("b0om") };
    }

    if(switch_lane())
    {
        std::string left("Left");
        std::string right("Right");
        if (model.state.curr_lane_index == 0)
        {
            // all teh right friends and all teh right choices
            return { make_switch(right) };
        }
        else
        {
            // everything you own in a box to teh left
            return { make_switch(left) };
        }
    }

    return { make_throttle(throttle) };
}

bool game_logic::switch_lane()
{
    bool do_switch = false;

     // && model.state.curr_piece_index != model.state.prev_piece_index)
    if(model.next_piece()->can_switch && model.track.switch_calculated != model.state.curr_piece_index)
    {
        model.track.switch_calculated = model.state.curr_piece_index;

        // we are left
        if (model.state.curr_lane_index == 0)
        {
             if (model.state.curr_piece_index < 6)
                 do_switch = true;

             if (model.state.curr_piece_index > 16)
                 do_switch = true;
         }

        // we are right
         else
        {
             if (model.state.curr_piece_index > 5 && model.state.curr_piece_index < 16)
                 do_switch = true;
         }
    }

    /*
    if(do_switch)
        std::cout << ">>> SWITCH" << std::endl;
    */

    return do_switch;
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
    std::cout << "Someone crashed" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
    std::cout << "Race ended" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
    std::cout << "Error: " << data.to_string() << std::endl;
    return { make_ping() };
}

////////////////////////
// Undefined Messages //
////////////////////////
game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
    /*
    */

    std::cout << "your car" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
    /*
    */

    std::cout << "game init" << std::endl;

    eRegionType prev_piece_type;

    // race
    jsoncons::json j_race           = data["race"];
    jsoncons::json j_cars           = j_race["cars"];
    jsoncons::json j_race_session   = j_race["raceSession"];
    jsoncons::json j_track          = j_race["track"];
    jsoncons::json j_lanes          = j_track["lanes"];
    jsoncons::json j_pieces         = j_track["pieces"];
    //debug_helper(j_cars);

    if(j_race_session.has_member("laps"))
        model.track.num_laps            = j_race_session["laps"].as<int>();
    if(j_race_session.has_member("maxLapTimeMs"))
        model.track.max_lap_time_ms     = j_race_session["maxLapTimeMs"].as<int>();
    if(j_race_session.has_member("quickRace"))
        model.track.is_quick_race       = j_race_session["quickRace"].as<bool>();

    model.track.id          = j_track["id"].as<std::string>();
    model.track.num_lanes   = j_lanes.size();
    model.track.lane_deltas.reserve(model.track.num_lanes);
    for (size_t l = 0; l < model.track.num_lanes; ++l)
    {
        int lane_index = j_lanes[l]["index"].as<int>();
        model.track.lane_deltas[lane_index] = j_lanes[l]["distanceFromCenter"].as<double>();
    }

    // pieces
    //debug_helper(j_pieces);
    model.track.num_pieces = j_pieces.size();
    model.track.sectors.reserve(model.track.num_pieces);
    model.track.sectors_lane_lengths.reserve(model.track.num_pieces);
    for (size_t p = 0; p < model.track.num_pieces; ++p)
    {
        sPiece tmp_piece;
        tmp_piece.angle        = .0;
        tmp_piece.radius       = .0;
        tmp_piece.length       = .0;
        tmp_piece.can_switch   = false;
        tmp_piece.curved       = false;

        // angle
        const auto& elem = j_pieces[p];
        if(elem.has_member("angle"))
        {
            tmp_piece.curved = true;
            tmp_piece.angle = elem["angle"].as<double>();
        }
        // radius
        if(elem.has_member("radius"))
        {
            tmp_piece.curved = true;
            tmp_piece.radius = static_cast<double>(elem["radius"].as<int>());
        }
        // switch lanes possible
        if(elem.has_member("switch"))
            tmp_piece.can_switch = elem["switch"].as<bool>();
        // piece length
        if(elem.has_member("length"))
            tmp_piece.length = elem["length"].as<double>();

        // save current piece info
        model.track.sectors[p] = tmp_piece;

        // save corrected lengths for all lanes on all pieces
        std::vector<double> tmp_lane_lengths;
        for(int l = 0; l < model.track.num_lanes; ++l)
        {
            if(!tmp_piece.curved)
            {
                tmp_lane_lengths.push_back(tmp_piece.length);
            }
            else
            {
                tmp_lane_lengths.push_back(model.track.piece_length(p, l));
            }
        }
        model.track.sectors_lane_lengths.push_back(tmp_lane_lengths);


        // create regions
        eRegionType tmp_piece_type = eUndef;
        if(tmp_piece.curved)
        {
            if(tmp_piece.angle < .0)
                tmp_piece_type = eLeftCurve;
            else
                tmp_piece_type = eRightCurve;
        }
        else
            tmp_piece_type = eStraight;

        int prev_ndx = model.track.regions.size()-1;

        // 1st piece or new type
        if(p == 0 || (prev_piece_type != tmp_piece_type))
        {
            sRegion tmp_region;
            if (tmp_piece.curved)
            {
                tmp_region.total_angle = model.track.sectors[p].angle;
            }
            else
            {
                tmp_region.total_angle = .0;
            }

            tmp_region.piece_index_start    = p;
            tmp_region.piece_index_stop     = tmp_region.piece_index_start + 1;
            tmp_region.total_pieces         = 1;

            tmp_region.region_type  = tmp_piece_type;
            prev_piece_type         = tmp_piece_type;
            model.track.regions.push_back(tmp_region);
        }
        // same type as before
        else
        {
            if (tmp_piece.curved)
                model.track.regions[prev_ndx].total_angle += model.track.sectors[p].angle;

            model.track.regions[prev_ndx].piece_index_stop++;
            model.track.regions[prev_ndx].total_pieces++;
        }
        tmp_piece.region_ndx = prev_ndx;
    }

    for (size_t r = 0; r < model.track.regions.size(); ++r)
    {
        std::string region_type_str;
        if(model.track.regions[r].region_type == eStraight)
            region_type_str = "S - ";
        else if(model.track.regions[r].region_type == eLeftCurve)
            region_type_str = "L - ";
        else if(model.track.regions[r].region_type == eRightCurve)
            region_type_str = "R - ";
        else
            region_type_str = "U - ";

        std::cout << region_type_str
        << model.track.regions[r].total_pieces << ", "
        << model.track.regions[r].piece_index_start << ", "
        << model.track.regions[r].piece_index_stop << ", "
        << model.track.regions[r].total_angle
        << std::endl;
    }

    if(model.track.num_pieces > 0)
        model.track.is_initialized = true;

    return { make_ping() };
}

game_logic::msg_vector game_logic::on_turbo_available(const jsoncons::json& data)
{
    model.make_turbo_available();
    /*

    */
    std::cout << "turbo available" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_finish(const jsoncons::json& data)
{
    /*

    */
    std::cout << "finished" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_lap_finished(const jsoncons::json& data)
{
    /*

    */
    model.make_can_turbo();
    std::cout << "lap finished" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_tournament_end(const jsoncons::json& data)
{
    /*

    */
    std::cout << "tournament ended" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& data)
{
    std::cout << "spawned, pfffffffff" << std::endl;
    return { make_ping() };
}
